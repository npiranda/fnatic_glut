/*********************************************************/
/* Beno�t Piranda          Universit� de Franche-Comt�   */
/* Copyright 2018                                        */
/*********************************************************/
#ifdef _WIN32
	#include <windows.h>
#endif
#include <iostream>
#include <string>
#include <GL/freeglut.h>
#include <math.h>
#include "objLoader.h"

using namespace std;

/*********************************************************/
/* prototypes                                            */
static void initGL();
static void reshapeFunc(int,int);
static void drawFunc();
static void kbdFunc(unsigned char,int,int);
static void mouseFunc(int button,int state,int x,int y);
static void motionFunc(int x,int y);
static void idleFunc();
static void quit();

/*********************************************************/
/* global variables                                      */
int width=1024,height=800; // initial size of the screen
const float cameraPos[] = {5.0f,20.0f,800.0f};
float cameraTheta=0.0f, cameraPhi=0.0f, cameraDist=100.0f; // spherical coordinates of the point of view
float rotationAngle=0; // rotation angle of the teapot /z
int mouseX0,mouseY0;

ObjLoader::ObjLoader *nael=NULL;
ObjLoader::ObjData *head;
ObjLoader::ObjData *hair;
ObjLoader::ObjData *body;
ObjLoader::ObjData *pelvis;
ObjLoader::ObjData *leftFeet;
ObjLoader::ObjData *rightFeet;
ObjLoader::ObjData *leftArm;
ObjLoader::ObjData *rightArm;
ObjLoader::ObjData *leftHand;
ObjLoader::ObjData *rightHand;

int main(int argc, char** argv) {
	glutInit(&argc, argv);

	initGL();
//	glutFullScreen();
//  glutSetCursor(GLUT_CURSOR_NONE); // allow to hide cursor

    nael = new ObjLoader::ObjLoader("models","nael.obj");

	head = nael->getObjectByName("head");
	hair = nael->getObjectByName("hair");
	body = nael->getObjectByName("body");
	pelvis = nael->getObjectByName("pelvis");
	leftFeet = nael->getObjectByName("left_feet");
	rightFeet = nael->getObjectByName("right_feet");
    leftArm = nael->getObjectByName("left_arm");
	rightArm = nael->getObjectByName("right_arm");
    leftHand = nael->getObjectByName("left_hand");
	rightHand = nael->getObjectByName("right_hand");

  /* bind reshape function */
	glutReshapeFunc(reshapeFunc);
  /* bind drawing function */
	glutDisplayFunc(drawFunc);
  /* bind mouse click function */
	glutMouseFunc(mouseFunc);
  /* bind mouse motion function */
    glutMotionFunc(motionFunc); // drag
	//glutPassiveMotionFunc(passiveMotionFunc);
  /* bind key pressed function */
	glutKeyboardFunc(kbdFunc);
  /* bind special key pressed function */
//  glutSpecialFunc(kbdSpecialFunc);
  /* bind idle function */
	glutIdleFunc(idleFunc);
  /* bind close function */
    glutCloseFunc(quit);

	glutMainLoop();

  return 0;
}

/*********************************************************/
/* frame drawing function                                */
static void drawFunc(void) {
	static GLfloat red[4] = { 1.0f, 0.0f, 0.0f, 1.0f}; // red color material
	static GLfloat green[4] = { 0.0f, 1.0f, 0.0f, 1.0f}; // red color material
	static GLfloat blue[4] = { 0.0f, 0.0f, 1.0f, 1.0f}; // red color material
	static GLfloat grey[4] = { 0.8f, 0.8f, 0.8f, 1.0f}; // red color material
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    double x = cameraDist*cos(cameraTheta)*cos(cameraPhi);
    double y  = cameraDist*sin(cameraTheta)*cos(cameraPhi);
    double z = cameraDist*sin(cameraPhi);

	GLfloat pos[4] = { 0.0f, 1.0f, 100.0f, 1.0f}; // position
    glLightfv(GL_LIGHT0, GL_POSITION, pos );

    gluLookAt(x,y,z,0,0,0,0,0,1.0);

/*	GLfloat pos[4] = { -1.0f, -8.0f, 6.0f, 1.0f}; // position
    glLightfv(GL_LIGHT0, GL_POSITION, pos );*/

    glPushMatrix();
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,blue);
    glutSolidCylinder(0.2,20.0,20,5);
    glPushMatrix();
    glRotatef(-90.0,1,0,0);
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,green);
    glutSolidCylinder(0.2,20.0,20,5);
    glPopMatrix();
    glRotatef(90.0,0,1,0);
    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,red);
    glutSolidCylinder(0.2,20.0,20,5);
    glPopMatrix();

    glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,grey);
    //  glRotatef(rotationAngle,0.0f,0.0f,1.0f);

    glPushMatrix();
        //glTranslatef(0,0,11.0);
		pelvis->glDraw();

		glPushMatrix();
            glTranslatef(0,-1.223,-3.2);
            glRotatef(rotationAngle,0.0,0.0,1.0f);
            glTranslatef(0,1.223,3.2);
            head->glDraw();
            hair->glDraw();
		glPopMatrix();

		body->glDraw();

		glPushMatrix();
            glTranslatef(0,-0.823,-3.2);
            glRotatef(rotationAngle,1.0f,0.0,0.0);
            glTranslatef(0,0.823,3.2);
            leftFeet->glDraw();
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0,-0.823,-3.2);
            glRotatef(-rotationAngle,1.0f,0.0,0.0);
            glTranslatef(0,0.823,3.2);
            rightFeet->glDraw();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(6.247,-1.184,10.6);
            glRotatef(-rotationAngle,1.0f,0.0,0.0);
            glTranslatef(-6.247,1.184,-10.6);
            leftArm->glDraw();
            leftHand->glDraw();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-6.247,-1.184,10.6);
            glRotatef(rotationAngle,1.0f,0.0,0.0);
            glTranslatef(6.247,1.184,-10.6);
            rightArm->glDraw();
            rightHand->glDraw();
        glPopMatrix();

	glPopMatrix();



	glPopMatrix();

    glutSwapBuffers();
}

/*********************************************************/
/* Window size update function                           */
/* width: width of the drawing area                      */
/* height: width of the drawing area                     */
static void reshapeFunc(int w,int h) {
	width=w;
	height=h;
	glViewport(0,0,width,height);
	// initialize Projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30.0,(double)width/(double)height,1.0,1000.0);
	// initialize ModelView matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/*********************************************************/
/* Animation function                                    */
static void idleFunc(void) {
	static int initTime = glutGet(GLUT_ELAPSED_TIME); // ms
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	double dt=(currentTime-initTime)/1000.0;

//	sprintf_s(chaineFPS,"FR : %lf",1.0/dt);
	initTime = currentTime;

	rotationAngle = 30.0*sin(currentTime*M_PI/1000.0);

	glutPostRedisplay();
}

/*********************************************************/
/* Key pressed function                                  */
/* c: key pressed character                              */
/* x,y: mouse coordinates                                */
static void kbdFunc(unsigned char c, int x, int y) {
	switch(c) {
        case 27: case 'q' : // quit
            glutLeaveMainLoop();
        break;
        case 'f' :
            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
        break;
        case 'F' :
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        break;
        case '+' :
            cameraDist+=0.10;
        break;
        case '-' :
            cameraDist-=0.10;
        break;
    }
	glutPostRedisplay();
}

/*********************************************************/
/* Mouse clicked function                                */
/* button: sum of pressed buttons id                     */
/* state: action                                         */
/* x,y: mouse coordinates                                */
static void mouseFunc(int button,int state,int x,int y) {
	mouseX0=x;
	mouseY0=y;
}

/*********************************************************/
/* Mouse move function (with button pressed)             */
/* x,y: mouse coordinates                                */
static void motionFunc(int x,int y) {
	cameraTheta += (x-mouseX0)*0.01;
	cameraPhi += (y-mouseY0)*0.01;

	mouseX0=x;
	mouseY0=y;
}

/*********************************************************/
/* Initialisation of the OPENGL window and parameters    */
static void initGL() {
    // create the window
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(width,height);
	glutCreateWindow("Master IOT");

	glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE); // auto-normalize normal vectors

    glClearColor(0.0,0.0,0.0,0.0); // background color

    glEnable(GL_LIGHTING);
    /* LIGHT0 light source parameters */
    GLfloat pos[4] = { 0.0f, 0.0f, 5.0f, 1.0f}; // position
    GLfloat light_diffuse[4] = {1.0, 1.0, 1.0, 1.0}; // colors
    GLfloat light_ambient[4] = {0.2, 0.2, 0.2, 1.0};

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, pos );
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_diffuse);
}

static void quit() {

}
