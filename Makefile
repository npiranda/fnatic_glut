OS = $(shell uname -s)

ifeq ($(OS),Darwin)
#MacOS (Assuming you are using the Clang compiler)
OSX_CCFLAGS = -DGL_DO_NOT_WARN_IF_MULTI_GL_VERSION_HEADERS_INCLUDED -Wno-deprecated-declarations -Wno-overloaded-virtual
LIBS = -L./ -L/usr/local/lib -lglut -framework GLUT -framework OpenGL -L/usr/X11/lib /usr/local/lib/libglut.dylib

else

LIBS = -L./ -L/usr/local/lib -L/usr/X11/lib -lglut -lGL -lGLU
CCFLAGS = -Wall
endif

CC = g++

SRCS = main.cpp objLoader.cpp
OBJS = $(SRCS:%.cpp=%.o)

APP = openGL_Nael

all: $(APP)

$(APP): $(OBJS)
	$(CC) -o $(APP) $(OBJS) $(LIBS)

main.o : main.cpp objLoader.h
	$(CC) -c main.cpp $(CCFLAGS)

objLoader.o : objLoader.cpp objLoader.h
	$(CC) -c objLoader.cpp $(CCFLAGS)

clean:
	rm *.o

mrproper: clean
	rm openGL_Nael
